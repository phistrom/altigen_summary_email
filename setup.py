#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup

setup(
    name='altigen-summary-email',
    version='0.1.1',
    packages=['altigen_summary_email'],
    install_requires=[
        'adodbapi',
        'altigen',
        'pywin32',
    ],
    url='https://bitbucket.org/phistrom/altigen_summary_email',
    license='MIT',
    author='Phillip Stromberg',
    author_email='phillip@4stromberg.com',
    description='Sends Altigen call logs and stats via email',
    scripts=['scripts/send_altigen_summary_email.py'],
    package_data={'altigen_summary_email': ['templates/*.jinja2']},
    zip_safe=False,
)
