# -*- coding: utf-8 -*-
"""
altigen-summary-email

Created by phillip on 10/10/2016
"""
from __future__ import print_function, unicode_literals

import altigen
try:
    from configparser import ConfigParser, NoOptionError  # Python 3
except ImportError:
    from ConfigParser import SafeConfigParser as ConfigParser, NoOptionError  # Python 2
from datetime import date
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from jinja2 import Environment, PackageLoader
import smtplib
import socket
from io import StringIO


class SummaryEmailSender(object):
    """
    Sends email summaries about Altigen
    """

    JINJA_ENVIRONMENT = Environment(loader=PackageLoader('altigen_summary_email'))

    def __init__(self, altigen_path, sender, smtp_host, port=0, local_hostname=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
        self._altigen = altigen.open(altigen_path)
        self.sender = sender
        self._smtp = smtplib.SMTP(host=smtp_host, port=port, local_hostname=local_hostname, timeout=timeout)

    def send_summary(self, recipients, day=None):
        top_holders = self._altigen.get_top_holders(day)
        top_talkers = self._altigen.get_top_talkers(day)
        call_log = self._altigen.get_day_call_log(day)
        if not day:
            day = date.today()
        plain_text = self._plain_text_summary(day, call_log, top_talkers, top_holders)
        print(plain_text)
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Phone Summary %s" % day.strftime("%Y-%m-%d")
        msg['From'] = self.sender
        msg['To'] = ', '.join(recipients)
        html_template = self.JINJA_ENVIRONMENT.get_template('mail_body.jinja2')
        html = html_template.render(call_log=call_log, top_holders=top_holders, top_talkers=top_talkers, round=round,
                                    day=day.strftime("%A, %B %d, %Y"))
        part1 = MIMEText(plain_text, 'plain')
        part2 = MIMEText(html, 'html')
        msg.attach(part1)
        msg.attach(part2)
        self._smtp.sendmail(self.sender, recipients, msg.as_string())

    def _plain_text_summary(self, day, call_log, talkers, holders):
        plain_text = StringIO()
        human_date = day.strftime("%A, %B %d, %Y")

        if not call_log:
            msg = "No calls were placed on {}".format(human_date)
            print(msg, file=plain_text)
            return plain_text.getvalue()

        print("Top 10 Talkers:\r\n", file=plain_text)
        print("{: <12} {: <20} {: >16}".format('Number', 'Name', 'Minutes Talking'), file=plain_text)
        print("=" * 50, file=plain_text)
        for r in self._altigen.get_top_talkers(day, limit=10):
            r['minutes'] = r['seconds'] / 60.0
            print('{number: <12} {name: <20} {minutes: >16.2f}'.format(**r), file=plain_text)

        print("\r\n\r\nTop 10 Longest Waited on Hold:\r\n", file=plain_text)
        print("{: <12} {: <20} {: >16}".format('Number', 'Name', 'Seconds Holding'), file=plain_text)
        print("=" * 50, file=plain_text)
        for r in self._altigen.get_top_holders(day, limit=10):
            print('{number: <12} {name: <20} {seconds: >16}'.format(**r), file=plain_text)

        print("\r\n\r\nCall Log for {}:\r\n".format(day.strftime(human_date)), file=plain_text)
        for rec in call_log:
            print("%s" % rec, file=plain_text)


        return plain_text.getvalue()

    @classmethod
    def from_config(cls, path):
        conf = ConfigParser()
        conf.read(path)
        altigen_path = conf.get('altigen', 'db_path')
        sender = conf.get('smtp', 'sender')
        host = conf.get('smtp', 'host')
        try:
            port = conf.getint('smtp', 'port')
        except NoOptionError:
            port = 0
        return cls(altigen_path=altigen_path, sender=sender, smtp_host=host, port=port)
