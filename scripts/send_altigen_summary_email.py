#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
altigen-summary-email

Created by phillip on 10/10/2016
"""

from argparse import ArgumentParser
import smtplib

from datetime import datetime

from altigen_summary_email.summary_email_sender import SummaryEmailSender


def _get_args():
    parser = ArgumentParser('altigen_summary_emailer')
    parser.add_argument('-c', '--config', default='./altigen.conf',
                        help="Location of a config file (default is ./altigen.conf)")
    parser.add_argument('-d', '--date',
                        help="The date in the format YYYY-MM-DD you want to get call log data for.")
    parser.add_argument('recipients', nargs='+',
                        help="A list of people to email")
    return parser.parse_args()


def main():
    args = _get_args()
    if args.date:
        args.date = datetime.strptime(args.date, '%Y-%m-%d').date()

    sendobj = SummaryEmailSender.from_config(args.config)
    sendobj.send_summary(args.recipients, args.date)

    return 0


if __name__ == "__main__":
    exit(main())
